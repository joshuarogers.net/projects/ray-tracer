import { Matrix } from "../matrix";
import { identity } from "../transformations";

export interface Shape {
    type: string,
    transformation: Matrix
}

export const shapeBase = (): Omit<Shape, 'type'> => ({
    transformation: identity()
});
