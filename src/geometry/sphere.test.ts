import { areEqual as areMatricesEqual } from "../matrix";
import { chain, identity, rotate, scale, translate } from "../transformations";

import { createPoint } from "../point";
import { createRay } from "../ray";
import { areEqual as areVectorsEqual, createVector, normalize } from "../vector";
import { createSphere, getIntersections, getSurfaceNormal } from "./sphere";

describe('Spheres', () => {
    test('exist', () => {
        expect(createSphere()).not.toBeNull()
    });

    test('have the identity matrix as their default transformation', () => {
        const sphere = createSphere();
        expect(areMatricesEqual(identity(), sphere.transformation)).toBeTruthy();
    });

    test('have two intersection points with rays starting in front of them', () => {
        const sphere = createSphere();
        const intersections = getIntersections(
            sphere,
            createRay(createPoint(0, 0, -5), createVector(0, 0, 1))
        );

        expect(intersections).toEqual([
            { time: 4, target: sphere },
            { time: 6, target: sphere }
        ]);
    });

    test('report two intersections for the tanget', () => {
        const sphere = createSphere();
        const intersectionDistances = getIntersections(
            sphere,
            createRay(createPoint(0, 1, -5), createVector(0, 0, 1))
        );

        expect(intersectionDistances).toEqual([
            { time: 5, target: sphere },
            { time: 5, target: sphere }
        ]);
    });

    test('are not hit my all rays', () => {
        const intersectionDistances = getIntersections(
            createSphere(),
            createRay(createPoint(0, 2, -5), createVector(0, 0, 1))
        );

        expect(intersectionDistances).toEqual([]);
    });

    test('have two intersection points with rays starting inside them', () => {
        const sphere = createSphere();
        const intersectionDistances = getIntersections(
            sphere,
            createRay(createPoint(0, 0, 0), createVector(0, 0, 1))
        );

        expect(intersectionDistances).toEqual([
            { time: -1, target: sphere },
            { time: 1, target: sphere }
        ]);
    });

    test('have two intersection points with rays starting behind them', () => {
        const sphere = createSphere();
        const intersectionDistances = getIntersections(
            sphere,
            createRay(createPoint(0, 0, 5), createVector(0, 0, 1))
        );

        expect(intersectionDistances).toEqual([
            { time: -6, target: sphere },
            { time: -4, target: sphere }
        ]);
    });

    test('intersect with a ray after scaling', () => {
        const ray = createRay(createPoint(0, 0, -5), createVector(0, 0, 1));
        const sphere = { ...createSphere(), transformation: scale(2, 2, 2) };
        const intersections = getIntersections(sphere, ray);

        expect(intersections.map(x => x.time)).toEqual([3, 7]);
    });

    test('intersect with a ray after translation', () => {
        const ray = createRay(createPoint(0, 0, -5), createVector(0, 0, 1));
        const sphere = { ...createSphere(), transformation: translate(5, 0, 0) };
        const intersections = getIntersections(sphere, ray);

        expect(intersections).toEqual([]);
    });
});

describe('Sphere surface normals', () => {
    test('can be calculated for points on the x-axis', () => {
        const sphere = createSphere();
        const normal = getSurfaceNormal(sphere, createPoint(1, 0, 0));
        expect(areVectorsEqual(normal, createVector(1, 0, 0))).toBeTruthy();
    });

    test('can be calculated for points on the y-axis', () => {
        const sphere = createSphere();
        const normal = getSurfaceNormal(sphere, createPoint(0, 1, 0));
        expect(areVectorsEqual(normal, createVector(0, 1, 0))).toBeTruthy();
    });

    test('can be calculated for points on the z-axis', () => {
        const sphere = createSphere();
        const normal = getSurfaceNormal(sphere, createPoint(0, 0, 1));
        expect(areVectorsEqual(normal, createVector(0, 0, 1))).toBeTruthy();
    });

    test('can be calculated for non-axial points', () => {
        const sqrt3 = Math.sqrt(3) / 3;
        const sphere = createSphere();
        const normal = getSurfaceNormal(sphere, createPoint(sqrt3, sqrt3, sqrt3));
        expect(areVectorsEqual(normal, createVector(sqrt3, sqrt3, sqrt3))).toBeTruthy();
    });

    test('are normalized to unit vectors', () => {
        const sqrt3 = Math.sqrt(3) / 3;
        const sphere = createSphere();
        const normal = getSurfaceNormal(sphere, createPoint(sqrt3, sqrt3, sqrt3));
        expect(areVectorsEqual(normal, normalize(normal))).toBeTruthy();
    });

    test('can be calculated for translated spheres', () => {
        const sphere = { ...createSphere(), transformation: translate(0, 1, 0) };
        const normal = getSurfaceNormal(sphere, createPoint(0, 1 + Math.SQRT1_2, -Math.SQRT1_2));
        expect(areVectorsEqual(normal, createVector(0, Math.SQRT1_2, -Math.SQRT1_2))).toBeTruthy();
    });

    test('can be calculated for spheres with complex transformations', () => {
        const sphere = {
            ...createSphere(),
            transformation: chain([rotate(0, 0, Math.PI / 5), scale(1, .5, 1)])
        };
        const normal = getSurfaceNormal(sphere, createPoint(0, Math.SQRT1_2, -Math.SQRT1_2));
        expect(areVectorsEqual(normal, createVector(0, 0.97014, -0.24254))).toBeTruthy();
    });
});

export {};
