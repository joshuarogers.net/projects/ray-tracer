import { type Intersection } from "../intersection";
import { invert, transpose } from "../matrix";
import { Point } from "../point";
import { type Ray } from "../ray";
import { projectPoint, projectRay, projectVector } from "../transformations";
import { createVector, dotProduct, normalize, Vector } from "../vector";
import { shapeBase, type Shape } from "./shape";

export const createSphere = (): Shape => ({
    ...shapeBase(),
    type: 'sphere'
});

export const getIntersections = (shape: Shape, ray: Ray): Intersection[] => {
    const worldToShapeProjection = invert(shape.transformation)!;
    const rayProjectedToShapeCoordinates = projectRay(ray, worldToShapeProjection);

    const [x, y, z] = rayProjectedToShapeCoordinates.position;
    const sphereToRayVector = createVector(x, y, z);

    // The naming a, b, and c appear to be poor names to start with, but in this
    // case, a, b, and c are used in the quadratic formula, typically defined in
    // terms of a, b, and c.

    // a: The dot product of the direction vector is the square of the magnitude of that vector
    const a = dotProduct(rayProjectedToShapeCoordinates.direction, rayProjectedToShapeCoordinates.direction);

    // b: The magnitude of sphereToRayVertex projected onto the ray direction vector. (Since
    //    the dot product represents a * b * cos(angleBetweenThem), we can visualize b as being
    //    twice the length of the base of the right triangle defined by using sphereToRayVector
    //    as the hypotenuse and the ray direction vector is the base.)
    const b = 2 * dotProduct(rayProjectedToShapeCoordinates.direction, sphereToRayVector);

    // c: One less than the square of the magnitude of the sphereToRay vector. Since our sphere
    //    is a unit sphere, this should make c be the distance from the surface of the sphere
    //    to the ray, rather than the sphere's center.
    const c = dotProduct(sphereToRayVector, sphereToRayVector) - 1;

    // The discriminant is the part of the quadratic formula under the square-root.
    // We are able to use it to quickly determine if there is an intersection. If the
    // value under the square root is negative, the result is an imaginary number, not
    // a distance. If the discriminant is zero, then our formula only has a single
    // solution, pointing towards this being a tangent line. If it is positive, then
    // there is both a front and back intersection.
    const discriminant = Math.pow(b, 2) - (4 * a * c);

    // So, what is...
    //   a: The length of a vector from origin to direction head.
    //   b: 
    //   c: The length of a vector between the surface of the sphere and the ray position
    if (discriminant < 0)
        return [];

    return [
        (-b - Math.sqrt(discriminant)) / (2 * a),
        (-b + Math.sqrt(discriminant)) / (2 * a)
    ].map(x => ({ time: x, target: shape }));
};

export const getSurfaceNormal = (shape: Shape, point: Point): Vector => {
    const worldToShapeProjection = invert(shape.transformation)!;
    const [x, y, z] = projectPoint(point, worldToShapeProjection);
    const shapeProjectedNormal = createVector(x, y, z);
    const transposedWorldToShapeProjection = transpose(worldToShapeProjection);

    return normalize(projectVector(shapeProjectedNormal, transposedWorldToShapeProjection));
};
