import { createSphere } from "./geometry/sphere";
import { getNextIntersection, type Intersection } from "./intersection";

describe('Next intersection', () => {
    test('can be found if all intersections are in front of the ray', () => {
        const sphere = createSphere();
        const intersections: Intersection[] = [
            { time: 2, target: sphere },
            { time: 1, target: sphere }
        ];

        const intersection = getNextIntersection(intersections);
        expect(intersection?.time).toBe(1);
    });

    test('can be found if some intersections are behind the ray', () => {
        const sphere = createSphere();
        const intersections: Intersection[] = [
            { time: -1, target: sphere },
            { time: 1, target: sphere }
        ];

        const intersection = getNextIntersection(intersections);
        expect(intersection?.time).toBe(1);
    });

    test('cannot be found if all intersections are behind the ray', () => {
        const sphere = createSphere();
        const intersections: Intersection[] = [
            { time: -1, target: sphere },
            { time: -2, target: sphere }
        ];

        const intersection = getNextIntersection(intersections);
        expect(intersection).toBeNull();
    });

    test('can be found regardless of initial order', () => {
        const sphere = createSphere();
        const intersections: Intersection[] = [
            { time: 5, target: sphere },
            { time: -3, target: sphere },
            { time: 4, target: sphere },
            { time: 16, target: sphere }
        ];

        const intersection = getNextIntersection(intersections);
        expect(intersection?.time).toBe(4);
    });
});

export {};
