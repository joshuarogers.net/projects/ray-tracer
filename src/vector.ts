import { isPracticallyEqual } from "./math";

export type Vector = [x: number, y: number, z: number];
export const createVector = (x: number, y: number, z: number): Vector => [x, y, z];

export const areEqual = ([x1, y1, z1]: Vector, [x2, y2, z2]: Vector): boolean =>
    isPracticallyEqual(x1, x2) && isPracticallyEqual(y1, y2) && isPracticallyEqual(z1, z2);

export const add = ([x1, y1, z1]: Vector, [x2, y2, z2]: Vector): Vector => createVector(x1 + x2, y1 + y2, z1 + z2);
export const subtract = ([x1, y1, z1]: Vector, [x2, y2, z2]: Vector): Vector => createVector(x1 - x2, y1 - y2, z1 - z2);
export const multiply = ([x, y, z]: Vector, scale: number): Vector => createVector(x * scale, y * scale, z * scale);
export const negate = ([x, y, z]: Vector) => createVector(-x, -y, -z);

export const magnitude = ([x, y, z]: Vector): number => Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
export const normalize = (vector: Vector): Vector => {
    const length = magnitude(vector);
    if (length === 0)
        throw new Error('Cannot normalize zero vector');
    return multiply(vector, 1 / length);
};

// Finds magnitude of vector1 after multiplying all components by the components of the other
// (relative to the projection of the first vector.) The result, when applied to a unit vector
// is the cos(angleBetweenTheVectors).
export const dotProduct = ([x1, y1, z1]: Vector, [x2, y2, z2]: Vector): number => x1 * x2 + y1 * y2 + z1 * z2;

// Finds vector perpendicular to the first two
export const crossProduct = ([x1, y1, z1]: Vector, [x2, y2, z2]: Vector): Vector =>
    createVector(y1 * z2 - z1 * y2, z1 * x2 - x1 * z2, x1 * y2 - y1 * x2);

// This one took a bit of mulling over to understand, but it works like this: let's say you
// have a surface of some kind. The shape does not matter. For every point on the surface,
// there is a `normal` vector that is perpendicular to a plane tangent to that point. So, for
// this point, if I were to throw a ball along this vector at the surface, and it bounced back,
// it would bounce back along the same vector that it in came by. Since the normal is perpendicular
// to the tangent at that point, regardless of the angle of approach, some multiple of the normal
// vector could be applied to the approach direction such that only the component that would have
// caused intersection would be zeroed but that all other components are maintained (when viewed
// from the local coordinate system of the plane, not necessarily with the world coordinates.)
//
// If this is true, then the next question would be "but what multiple do we apply?" The answer
// to that is "the amount needed to cancel out the component of the direction vector that points
// toward the normal vector". That seems intuitive enjoy, but exactly how much is that? At this
// point, it becomes super helpful to visualize a triangle defined by our two vectors and our plane
// where the direction is the hypotenuse, the normal is adjacent, and our tangent plane is the
// opposite.
//
//     Plane
//  N |-----/ D
//  o |    / i
//  r |   / r
//  m |  / e
//  a | / c
//  l |/ t
//
// Given the above, intuition tells us that the magnitude of the direction vector in the direction
// of (or rather, projected onto) the normal should be the length of direction times the ratio of
// normal over direct. Reaching back to trig, we might remember this as COS(angle) * length. This
// happens to be what the dot product gives us. So, we can answer "but what multiple do we apply",
// with "the negative of the dot product of the direction and normal vectors."
//
// Now that we know how to zero the component of the direction vector that runs parallel to the
// normal vector, we're ready to tackle reflections. So, at that this point we ask "what is a
// reflection?" After thinking about it for a bit, I think we could say that a vector is reflected
// IFF (once again, in the local coordinate system of the plane), the vector component pointing
// towards the plane is negated but all other components are unchanged. This also means that the
// magnitude will be the same and that the angle of entry will mirror the angle of escape.
//
// So, what complex math is required for this? We double the magnitude needed to simply cancel the
// component pointing towards the tanget plane. That's it. Since subtracting one times the value was
// enough to fully cancel out the component, doubling the value being subtracted will be enough to
// negate it. And, since this is only affecting one component (coordinate systems...) the new vector
// will have the same magnitude and will mirror our angle of entry, as expected.
//
// Thus, the direction of a reflection is the original direction minus two times the dot product of the
// our two vectors applied to the normal vector.
export const getReflection = (direction: Vector, normal: Vector): Vector =>
    subtract(direction, multiply(normal, 2 * dotProduct(direction, normal)));
