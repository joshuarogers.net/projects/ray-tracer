import { areEqual as arePointsEqual, createPoint } from "./point";
import { createRay, positionAtTime } from "./ray";
import { areEqual as areVectorsEqual, createVector } from "./vector";

describe('Rays', () => {
    test('have a position', () => {
        const position = createPoint(1, 3, 3);
        const ray = createRay(position, createVector(0, 0, 1));
        expect(arePointsEqual(position, ray.position)).toBeTruthy();
    });

    test('have a vector', () => {
        const vector = createVector(1, 3, 3);
        const ray = createRay(createPoint(0, 0, 1), vector);
        expect(areVectorsEqual(vector, ray.direction)).toBeTruthy();
    });

    test('have a predictable position based on time', () => {
        const ray = createRay(createPoint(1, 2, 4), createVector(2, -3, 1));
        const position = positionAtTime(ray, 3.5);
        const target = createPoint(8, -8.5, 7.5);

        expect(arePointsEqual(position, target)).toBeTruthy();
    });
});

export {}
