import { EPSILON } from "./math";
import { add, areEqual, createVector, crossProduct, dotProduct, getReflection, magnitude, multiply, negate, normalize, subtract } from "./vector";

describe('Vectors', () => {
    test('should be considered equal if they are nearly identical', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(1 + EPSILON / 2, 2 + EPSILON / 2, 3 + EPSILON / 2);
        expect(areEqual(vector1, vector2)).toBeTruthy();
    });

    test('should be considered equal if they are nearly identical', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(1 - EPSILON / 2, 2 - EPSILON / 2, 3 - EPSILON / 2);
        expect(areEqual(vector1, vector2)).toBeTruthy();
    });

    test('should not be considered equal if "x" is not nearly identical', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(1 + EPSILON * 2, 2, 3);
        expect(areEqual(vector1, vector2)).toBeFalsy();        
    });

    test('should not be considered equal if "y" is not nearly identical', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(1, 2 + EPSILON * 2, 3);
        expect(areEqual(vector1, vector2)).toBeFalsy();        
    });

    test('should not be considered equal if "z" is not nearly identical', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(1, 2, 3 + EPSILON * 2);
        expect(areEqual(vector1, vector2)).toBeFalsy();        
    });

    test('should be ordered as x, y, z', () => {
        const [x, y, z] = createVector(1, 2, 3);
        expect([x, y, z]).toEqual([1, 2, 3]);
    });

    test('should be addable', () => {
        const vector1 = createVector(1, 2, 4);
        const vector2 = createVector(2, 5, -2);
        const [x, y, z] = add(vector1, vector2);
        expect([x, y, z]).toEqual([3, 7, 2]);
    });

    test('should be subtractable', () => {
        const vector1 = createVector(1, 3, 5);
        const vector2 = createVector(1, 4, 2);
        const [x, y, z] = subtract(vector1, vector2);
        expect([x, y, z]).toEqual([0, -1, 3]);
    });

    test('should be able to be multiplied by a scalar', () => {
        const vector = createVector(1, 4, 6);
        const [x, y, z] = multiply(vector, 1.5);
        expect([x, y, z]).toEqual([1.5, 6, 9]);
    });

    test('should have magnitude', () => {
        const length = magnitude(createVector(2, 3, 5));
        expect(length).toBeCloseTo(6.164, 3);
    });

    test('should have a negation', () => {
        const vector = createVector(2, 4, 6);
        const [x, y, z] = negate(vector);
        expect([x, y, z]).toEqual([-2, -4, -6]);
    });

    test('should maintain direction when normalizing', () => {
        const vector1 = normalize(createVector(1, 2, 3));
        const vector2 = createVector(0.26726, 0.53452, 0.80178);
        expect(areEqual(vector1, vector2)).toBeTruthy();
    });

    test('should have a magnitude of 1 after normalizing', () => {
        expect(magnitude(normalize(createVector(1, 2, 3)))).toBeCloseTo(1, EPSILON);
    });

    test('should show error when normalizing if it is zero length', () => {
        expect(() => normalize(createVector(0, 0, 0))).toThrow('Cannot normalize zero vector');
    });

    test('should support dot product', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(2, 3, 4);
        expect(dotProduct(vector1, vector2)).toBe(20);
    });

    test('cross product should point to perpendicular vector', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(2, 3, 4);
        expect(areEqual(crossProduct(vector1, vector2), createVector(-1, 2, -1))).toBeTruthy();
    });

    test('cross product should point in opposite direction when swapping vector order', () => {
        const vector1 = createVector(1, 2, 3);
        const vector2 = createVector(2, 3, 4);
        expect(areEqual(crossProduct(vector1, vector2), negate(crossProduct(vector2, vector1)))).toBeTruthy();
    });

    test('can be reflected off a flat surface', () => {
        const direction = createVector(1, -1, 0);
        const normal = createVector(0, 1, 0);
        expect(areEqual(getReflection(direction, normal), createVector(1, 1, 0))).toBeTruthy();
    });

    test('can be reflected off a slanted surface', () => {
        const direction = createVector(0, -1, 0);
        const normal = createVector(Math.SQRT2 / 2, Math.SQRT2 / 2, 0);
        expect(areEqual(getReflection(direction, normal), createVector(1, 0, 0))).toBeTruthy();
    });
});

export {};
