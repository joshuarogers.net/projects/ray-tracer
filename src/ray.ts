import { type Point, translatePoint } from "./point";
import { multiply, type Vector } from "./vector";

export interface Ray {
    position: Point,
    direction: Vector
}

export const createRay = (position: Point, direction: Vector): Ray => ({
    position,
    direction
});

export const positionAtTime = (ray: Ray, time: number) => translatePoint(ray.position, multiply(ray.direction, time));
