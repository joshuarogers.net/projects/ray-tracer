import { range, sum } from "lodash";
import { EPSILON } from "./math";

export interface Matrix {
    getWidth(): number,
    getHeight(): number,

    getValue(x: number, y: number): number,
    setValue(x: number, y: number, value: number): void
}

const createMatrixFromData = (values: number[][]): Matrix => {
    const height = values.length;
    if (height === 0)
        throw new Error('At least one row is required to create a matrix');

    const width = values[0].length;    
    const areAllRowsEquallySized = values.filter(x => x.length !== width).length === 0;

    if (!areAllRowsEquallySized)
        throw new Error('Cannot create matrix with variable row sizing');

    if (width === 0)
        throw new Error('At least one column is required to create a matrix');

    return {
        getWidth: () => width,
        getHeight: () => height,

        getValue: (x, y) => {
            if (x < 0 || y < 0 || x >= width || y >= height)
                throw new Error(`Coordinate (${x}, ${y}) out of range`);
            return values[y][x];
        },
        setValue: (x, y, value) => {
            if (x < 0 || y < 0 || x >= width || y >= height)
                throw new Error(`Coordinate (${x}, ${y}) out of range`);
            return values[y][x] = value;
        }
    };
};

export const createMatrix = (valuesOrWidth: number[][] | number, height?: number): Matrix => {
    if (typeof valuesOrWidth === 'object')
        return createMatrixFromData(valuesOrWidth);

    const calculatedWidth = valuesOrWidth;
    const calculatedHeight = height !== undefined
        ? height
        : valuesOrWidth;

    const data = range(0, calculatedHeight, 1).map(() => range(0, calculatedWidth, 1).map(() => 0));
    return createMatrixFromData(data);
};

export const createIdentityMatrix = (size: number): Matrix => {
    if (size <= 0)
        throw new Error('Identity matrix size is not valid');

    const matrix = createMatrix(size);
    for (let y = 0; y < size; y++)
        for (let x = 0; x < size; x++)
            matrix.setValue(x, y, x === y ? 1 : 0);

    return matrix;
};

export const areEqual = (matrix1: Matrix | null, matrix2: Matrix | null): boolean => {
    if (matrix1 === null || matrix2 === null)
        return false;

    if (matrix1.getWidth() !== matrix2.getWidth() || matrix1.getHeight() !== matrix2.getHeight())
        return false;

    const width = matrix1.getWidth();
    const height = matrix1.getHeight();

    for (let y = 0; y < height; y++)
        for (let x = 0; x < width; x++)
            if (Math.abs(matrix1.getValue(x, y) - matrix2.getValue(x, y)) > EPSILON)
                return false;

    return true;
}

export const multiply = (matrix1: Matrix, matrix2: number | Matrix): Matrix => {
    if (typeof matrix2 === 'number') {
        const result = createMatrix(matrix1.getWidth(), matrix1.getHeight());
        for (let y = 0; y < matrix1.getHeight(); y++)
            for (let x = 0; x < matrix1.getWidth(); x++)
                result.setValue(x, y, matrix1.getValue(x, y) * matrix2);

        return result;
    }

    if (matrix1.getWidth() !== matrix2.getHeight())
        throw new Error('Matrices are not of compatible sizes for multiplication');

    const width = matrix2.getWidth();
    const height = matrix1.getHeight();
    const commonLength = matrix1.getWidth();

    const result = createMatrix(width, height);

    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            let total = 0;
            for (let i = 0; i < commonLength; i++)
                total += (matrix1.getValue(i, y) * matrix2.getValue(x, i));
            result.setValue(x, y, total);
        }
    }

    return result;
};

export const transpose = (source: Matrix): Matrix => {
    const width = source.getHeight();
    const height = source.getWidth();

    const matrix = createMatrix(width, height);
    for (let y = 0; y < height; y++)
        for (let x = 0; x < width; x++)
            matrix.setValue(x, y, source.getValue(y, x));

    return matrix;
};

export const invert = (source: Matrix): Matrix | null => {
    if (source.getWidth() !== source.getHeight())
        return null;

    // Wraps the source matrix in a way that simulates the removal of a given column and row.
    type MatrixMinorView = Pick<Matrix, "getWidth" | "getValue">;
    const createMinorSubmatrix = (matrix: MatrixMinorView, excludeX: number, excludeY: number): MatrixMinorView => ({
        getWidth: () => matrix.getWidth() - 1,
        getValue: (x, y) => matrix.getValue(x < excludeX ? x : x + 1, y < excludeY ? y : y + 1)
    });

    // Gets the determinant of a matrix by recursively calculating the determinant of submatrices until reaching a 2x2.
    const getDeterminant = (matrix: MatrixMinorView): number => {
        if (matrix.getWidth() === 2)
            return (matrix.getValue(0, 0) * matrix.getValue(1, 1)) - (matrix.getValue(1, 0) * matrix.getValue(0, 1));

        return sum(range(0, matrix.getWidth())
            .map(x => (matrix.getValue(x, 0) * getDeterminant(createMinorSubmatrix(matrix, x, 0)) * (x % 2 === 0 ? 1 : -1))));
    }

    // The matrix of cofactors is actually just a matrix of each positions determinant where every determinant has
    // had its sign flipped.
    const cofactors = createMatrix(source.getWidth(), source.getHeight());
    for (let y = 0; y < source.getHeight(); y++)
        for (let x = 0; x < source.getWidth(); x++)
            cofactors.setValue(x, y, getDeterminant(createMinorSubmatrix(source, x, y)) * ((x + y) % 2 === 0 ? 1 : -1));

    // Rather than calling getDeterminant on the source matrix and causing a large amount of duplicate effort, we
    // can figure out the determinant of the source matrix by being aware that the determinant calculation, should
    // we have called it, would have only differed by multiplying and the first row of the source with the first
    // row of the values that we have in the cofactor submatrix.
    const determinant = sum(range(0, source.getWidth()).map(x => source.getValue(x, 0) * cofactors.getValue(x, 0)));

    // If the determinant is null, this is a singular matrix, meaning that it has no inverse.
    if (determinant === 0)
        return null;

    const adjoint = transpose(cofactors);
    return multiply(adjoint, 1.0 / determinant);
};
