import { Color, createColor } from "./color";
import { range } from "lodash";

export interface Canvas {
    setPixel(x: number, y: number, color: Color): void,
    getPixel(x: number, y: number): Color,

    getWidth(): number,
    getHeight(): number
};

export const createCanvas = (width: number, height: number): Canvas => {
    const pixels: Color[][] = range(0, height)
        .map(() => range(0, width).map(x => createColor(0, 0, 0)));

    return {
        setPixel: (x, y, color) => pixels[Math.floor(y)][Math.floor(x)] = color,
        getPixel: (x, y) => pixels[Math.floor(y)][Math.floor(x)],

        getWidth: () => width,
        getHeight: () => height
    };
};
