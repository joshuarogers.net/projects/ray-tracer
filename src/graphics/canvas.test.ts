import { createCanvas } from "./canvas";
import { createColor } from "./color";

describe('Canvas', () => {
    test('initializes to black', () => {
        const canvas = createCanvas(800, 600);
        expect(canvas.getPixel(0, 0)).toEqual([0, 0, 0]);
    });

    test('can be drawn', () => {
        const canvas = createCanvas(800, 600);
        canvas.setPixel(0, 0, createColor(1, 0, 1));
        expect(canvas.getPixel(0, 0)).toEqual([1, 0, 1]);
    });

    test('maintains width', () => {
        const canvas = createCanvas(800, 600);
        expect(canvas.getWidth()).toBe(800);
    });

    test('maintains height', () => {
        const canvas = createCanvas(800, 600);
        expect(canvas.getHeight()).toBe(600);
    });
});
