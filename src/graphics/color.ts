export type Color = [red: number, green: number, blue: number];

export const createColor = (red: number, green: number, blue: number): Color => [red, green, blue];

export const add = ([red1, green1, blue1]: Color, [red2, green2, blue2]: Color): Color =>
    createColor(red1 + red2, green1 + green2, blue1 + blue2);

export const subtract = ([red1, green1, blue1]: Color, [red2, green2, blue2]: Color): Color =>
    createColor(red1 - red2, green1 - green2, blue1 - blue2);

export const multiply = ([red1, green1, blue1]: Color, color2: Color | number): Color => {
    const [red2, green2, blue2] = (typeof color2 === 'number')
        ? [color2, color2, color2]
        : color2;

    return createColor(red1 * red2, green1 * green2, blue1 * blue2);
};
