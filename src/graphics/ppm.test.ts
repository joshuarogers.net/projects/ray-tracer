import { flatten } from "lodash";
import { createCanvas } from "./canvas";
import { createColor } from "./color";
import { exportToBlob } from "./ppm";

const width = 64;
const height = 48;

describe('ppm', () => {
    let ppm: string[];

    beforeAll(() => {
        const canvas = createCanvas(width, height);

        // Create a sort-of checkerboard pattern
        for (let x = 0; x < width; x++)
            for (let y = 0; y < height; y++)
                if ((x + y) % 2 === 0)
                    canvas.setPixel(x, y, createColor(1, .125, .5));

        ppm = exportToBlob(canvas).split('\n');
    });

    test('line 1 should be the PPM magic number', () => {
        expect(ppm[0]).toBe('P3');
    });

    test('line 2 should be the dimensions', () => {
        expect(ppm[1]).toBe('64 48');
    });

    test('line 3 should be the max color component value', () => {
        expect(ppm[2]).toBe('255');
    });

    test('line 4 starts with color data', () => {
        expect(ppm[3]).toMatch('255 31 127 0 0 0');
    });

    test('color data should all be less than the max component value', () => {
        const maxColorValue = parseInt(ppm[2]);
        const allColorValues = flatten(ppm.slice(3).map(x => x.split(' '))).map(x => parseInt(x));
        expect(allColorValues.filter(x => x > maxColorValue)).toEqual([]);
    });

    test('color data should all be positive', () => {
        const allColorValues = flatten(ppm.slice(3).map(x => x.split(' '))).map(x => parseInt(x));
        expect(allColorValues.filter(x => x < 0)).toEqual([]);
    });

    test('color data should consist of three numbers per pixel', () => {
        const allColorValues = flatten(ppm.slice(3).map(x => x.split(' '))).map(x => parseInt(x));
        expect(allColorValues).toHaveLength(width * height * 3);
    });

    test('should not exceed 70 characters per line', () => {
        expect(ppm.filter(x => x.length > 70)).toEqual([]);
    })
});