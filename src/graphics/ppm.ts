import { Canvas } from "./canvas";
import { clamp, flatten, floor, range } from "lodash";
import { Color } from "./color";

const maxColorComponent = 255;
const renderColor = (color: Color): number[] => 
    color.map(x => clamp(floor(x * maxColorComponent), 0, maxColorComponent));

export const exportToBlob = (canvas: Canvas): string => {
    const width = canvas.getWidth();
    const height = canvas.getHeight();

    const header = [
        // Magic Number for PPM
        'P3',

        // Dimensions
        `${width} ${height}`,

        // Maximum intensity pixel component value
        `${maxColorComponent}`
    ];

    const renderRow = (y: number): string => {
        const colorComponents = flatten(range(0, width)
            .map(x => canvas.getPixel(x, y))
            .map(x => renderColor(x)))
            .map(x => x.toString());
        
        let currentColorComponentIndex = 0;
        const rows: string[] = [];
        while (currentColorComponentIndex < colorComponents.length) {
            let lineWidth = 0;
            let lineStart = currentColorComponentIndex;
            // We choose 66 due to the knowledge that our largest value supported is 3 characters and one space before it
            // This tells us that, as long as we are less than 66 characters, we can safely add any following value without
            // hitting 70.
            while (lineWidth < 66 && currentColorComponentIndex < colorComponents.length) {
                lineWidth += colorComponents[currentColorComponentIndex].length + 1; // Add one due to the space following each number
                currentColorComponentIndex++;
            }
            rows.push(colorComponents.slice(lineStart, currentColorComponentIndex).join(' '));
        }

        return rows.join('\n');
    };

    const colorData = range(0, height).map(renderRow);

    return header.concat(colorData).join('\n');
};
