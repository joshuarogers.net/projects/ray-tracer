import { add, createColor, multiply, subtract } from './color';

describe('Colors', () => {
    test('should be addable', () => {
        const color1 = createColor(.25, .5, .5);
        const color2 = createColor(.35, .5, .25);
        expect(add(color1, color2)).toEqual([.6, 1, .75]);
    });

    test('should be subtractable', () => {
        const color1 = createColor(.5, 1, .5);
        const color2 = createColor(.25, .5, .5);
        expect(subtract(color1, color2)).toEqual([.25, .5, 0]);
    });

    test('should be able to be multiplied by a scalar', () => {
        const color = createColor(.125, .25, .5);
        expect(multiply(color, 2)).toEqual([.25, .5, 1]);
    });

    test('should be able to be multiplied by another color', () => {
        const color1 = createColor(.5, .5, .5);
        const color2 = createColor(.5, .25, 1);
        expect(multiply(color1, color2)).toEqual([.25, .125, .5]);
    });
});