export const EPSILON = 0.00001;
export const isPracticallyEqual = (x: number, y: number): boolean => Math.abs(x - y) <= EPSILON;
