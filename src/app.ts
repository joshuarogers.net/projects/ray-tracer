import { writeFileSync } from 'fs';
import { createSphere, getIntersections } from './geometry/sphere';
import { type Canvas, createCanvas } from "./graphics/canvas";
import { createColor } from "./graphics/color";
import { exportToBlob } from "./graphics/ppm";
import { getNextIntersection } from './intersection';
import { createPoint } from "./point";
import { createRay } from './ray';
import { chain, scale, translate } from "./transformations";
import { createVector } from './vector';

export const exportToFile = (canvas: Canvas, file: string) => {
    const contents = exportToBlob(canvas);
    writeFileSync(file, contents);
};

const WIDTH = 800;
const HEIGHT = 600;

const canvas = createCanvas(WIDTH, HEIGHT);
const sphere = {
    ...createSphere(),
    transformation: chain([scale(320, 320, 400), translate(WIDTH / 2, HEIGHT / 2, 0)])
};

const color = createColor(1, 0, 1);
for (let y = 0; y < HEIGHT; y++) {
    for (let x = 0; x < WIDTH; x++) {
        const ray = createRay(createPoint(x, y, 0), createVector(0, 0, 1));
        const intersections = getIntersections(sphere, ray);
        const nextIntersection = getNextIntersection(intersections);

        if (nextIntersection) {
            canvas.setPixel(x, y, createColor(x / WIDTH, y / HEIGHT, nextIntersection.time / 320));
        }
    }
}

exportToFile(canvas, "/home/jrogers/Desktop/sphere.ppm");
