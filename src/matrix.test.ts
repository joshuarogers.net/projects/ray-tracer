import { EPSILON } from "./math";
import { areEqual, createIdentityMatrix, createMatrix, invert, Matrix, multiply, transpose } from "./matrix";

describe("Matrices", () => {
    let matrix: Matrix;

    beforeEach(() => {
        matrix = createMatrix([
            [1, 2, 3],
            [4, 5, 6]
        ]);
    })

    test('must have at least one row', () => {
        expect(() => createMatrix([])).toThrow('At least one row is required to create a matrix');
    });

    test('must have at least one column', () => {
        expect(() => createMatrix([
            []
        ])).toThrow('At least one column is required to create a matrix');
    });

    test('must have all columns sized the same', () => {
        expect(() => createMatrix([
            [1, 2, 3],
            [4, 5],
            [6]
        ])).toThrow('Cannot create matrix with variable row sizing');
    });

    test('have a width', () => {
        expect(matrix.getWidth()).toBe(3);
    });

    test('have a height', () => {
        expect(matrix.getHeight()).toBe(2);
    });

    test('have values that can be read', () => {
        expect(matrix.getValue(2, 1)).toBe(6);
    });

    test('have values that can be set', () => {
        matrix.setValue(2, 1, 8);
        expect(matrix.getValue(2, 1)).toBe(8);
    });

    test('throws an exception when trying to set a value with a negative X', () => {
        expect(() => matrix.setValue(-1, 0, 1)).toThrow('Coordinate (-1, 0) out of range');
    });

    test('throws an exception when trying to set a value with a negative Y', () => {
        expect(() => matrix.setValue(0, -1, 1)).toThrow('Coordinate (0, -1) out of range');
    });

    test('throws an exception when trying to set a value with an X beyond width', () => {
        expect(() => matrix.setValue(3, 0, 1)).toThrow('Coordinate (3, 0) out of range');
    });

    test('throws an exception when trying to set a value with a Y beyond height', () => {
        expect(() => matrix.setValue(0, 2, 1)).toThrow('Coordinate (0, 2) out of range');
    });

    test('throws an exception when trying to read a value with a negative X', () => {
        expect(() => matrix.getValue(-1, 0)).toThrow('Coordinate (-1, 0) out of range');
    });

    test('throws an exception when trying to read a value with a negative Y', () => {
        expect(() => matrix.getValue(0, -1)).toThrow('Coordinate (0, -1) out of range');
    });

    test('throws an exception when trying to read a value with an X beyond width', () => {
        expect(() => matrix.getValue(3, 0)).toThrow('Coordinate (3, 0) out of range');
    });

    test('throws an exception when trying to read a value with a Y beyond height', () => {
        expect(() => matrix.getValue(0, 2)).toThrow('Coordinate (0, 2) out of range');
    });

    test('are not equal if either matrix is null', () => {
        const matrix = createMatrix([
            [1, 2],
            [3, 4]
        ]);

        expect(areEqual(matrix, null)).toBeFalsy();
        expect(areEqual(null, matrix)).toBeFalsy();
        expect(areEqual(null, null)).toBeFalsy();
    });

    test('are not equal if they have differing widths', () => {
        const matrix1 = createMatrix([
            [1, 2],
            [3, 4]
        ]);

        const matrix2 = createMatrix([
            [1, 2, 0],
            [3, 4, 0]
        ]);

        expect(areEqual(matrix1, matrix2)).toBeFalsy();
    });

    test('are not equal if they have differing heights', () => {
        const matrix1 = createMatrix([
            [1, 2],
            [3, 4]
        ]);

        const matrix2 = createMatrix([
            [1, 2],
            [3, 4],
            [0, 0]
        ]);

        expect(areEqual(matrix1, matrix2)).toBeFalsy();
    });

    test('are equal if all cells are identical', () => {
        const matrix1 = createMatrix([
            [1, 2, 3],
            [6, 5, 4],
            [7, 8, 9]
        ]);

        const matrix2 = createMatrix([
            [1, 2, 3],
            [6, 5, 4],
            [7, 8, 9]
        ]);

        expect(areEqual(matrix1, matrix2)).toBeTruthy();
    });

    test('are equal if all cells are within EPSILON', () => {
        const matrix1 = createMatrix([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]);

        const HALF_EPSILON = EPSILON / 2;
        const matrix2 = createMatrix([
            [1 + HALF_EPSILON, 2 - HALF_EPSILON, 3 + HALF_EPSILON],
            [4 - HALF_EPSILON, 5 + HALF_EPSILON, 6 - HALF_EPSILON],
            [7 + HALF_EPSILON, 8 - HALF_EPSILON, 9 + HALF_EPSILON]
        ]);

        expect(areEqual(matrix1, matrix2)).toBeTruthy();
    });

    test('are unequal if any cell deviates by more than EPSILON', () => {
        const matrix1 = createMatrix([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]);

        const matrix2 = createMatrix([
            [1, 2, 3],
            [4, 5, 6 + EPSILON + EPSILON],
            [7, 8, 9]
        ]);

        expect(areEqual(matrix1, matrix2)).toBeFalsy();
    });

    test('can be multiplied by matrices of the same size', () => {
        const matrix1 = createMatrix([
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 8, 7, 6],
            [5, 4, 3, 2]
        ]);

        const matrix2 = createMatrix([
            [-2, 1, 2, 3],
            [3, 2, 1, -1],
            [4, 3, 6, 5],
            [1, 2, 7, 8]
        ]);

        const result = multiply(matrix1, matrix2);
        const expected = createMatrix([
            [20, 22, 50, 48],
            [44, 54, 114, 108],
            [40, 58, 110, 102],
            [16, 26, 46, 42]
        ]);

        expect(areEqual(result, expected)).toBeTruthy();
    });

    test('can be multiplied by matrices of other sizes', () => {
        const matrix1 = createMatrix([
            [1, 2, 3],
            [4, 5, 6]
        ]);

        const matrix2 = createMatrix([
            [7, 8],
            [9, 10],
            [11, 12]
        ]);

        const result = multiply(matrix1, matrix2);
        const expected = createMatrix([
            [58, 64],
            [139, 154]
        ]);

        expect(areEqual(result, expected)).toBeTruthy();
    });

    test('can only be multiplied if other width matches this height', () => {
        const matrix1 = createMatrix([
            [1, 2, 3],
            [4, 5, 6]
        ]);

        const matrix2 = createMatrix([
            [3, 4, 5],
            [6, 7, 8]
        ]);

        expect(() => multiply(matrix1, matrix2)).toThrow('Matrices are not of compatible sizes for multiplication');
    });

    test('can be created given a square size', () => {
        const matrix = createMatrix(4);
        expect(matrix.getWidth()).toBe(4);
        expect(matrix.getHeight()).toBe(4);
    });

    test('can be created given a rectangular size', () => {
        const matrix = createMatrix(4, 5);
        expect(matrix.getWidth()).toBe(4);
        expect(matrix.getHeight()).toBe(5);
    });

    test('cannot be created with a negative width', () => {
        expect(() => createMatrix(5, -3)).toThrow('At least one row is required to create a matrix');
    });

    test('cannot be created with a negative height', () => {
        expect(() => createMatrix(-3, 5)).toThrow('At least one column is required to create a matrix');
    });

    test('cannot be created with a zero width', () => {
        expect(() => createMatrix(0, 5)).toThrow('At least one column is required to create a matrix');
    });

    test('cannot be created with a zero height', () => {
        expect(() => createMatrix(5, 0)).toThrow('At least one row is required to create a matrix');
    });

    test('can be transposed', () => {
        const source = createMatrix([
            [1, 2, 3],
            [4, 5, 6]
        ]);

        const expected = createMatrix([
            [1, 4],
            [2, 5],
            [3, 6]
        ]);

        expect(areEqual(transpose(source), expected)).toBeTruthy();
    });

    test('can have an inverse', () => {
        const matrix = createMatrix([
            [8, -5 , 9, 2],
            [7, 5, 6, 1],
            [-6, 0, 9, 6],
            [-3, 0, -9, -4]
         ]);

         const inverse = createMatrix([
            [-0.15385, -0.15385, -0.28205, -0.53846],
            [-0.07692, 0.12308, 0.02564, 0.03077],
            [0.35897, 0.35897, 0.43590, 0.92308],
            [-0.69231, -0.6923, -0.76923, -1.92308]
         ]);

         expect(areEqual(invert(matrix), inverse)).toBeTruthy();
    });

    test('might not have an inverse', () => {
        const matrix = createMatrix([
            [2, 4, 6],
            [2, 0, 2],
            [6, 8, 14]
        ]);

        expect(invert(matrix)).toBeNull();
    });

    test('cannot be inverted if they are non-square', () => {
        const matrix = createMatrix([
            [1, 2, 3],
            [4, 5, 6]
        ]);
        expect(invert(matrix)).toBeNull();
    });
});

describe('Identity Matrices', () => {
    test('cannot be negative sized', () => {
        expect(() => createIdentityMatrix(-4)).toThrow('Identity matrix size is not valid');
    });

    test('cannot be zero sized', () => {
        expect(() => createIdentityMatrix(0)).toThrow('Identity matrix size is not valid');
    });

    test('are 1 on the diagonal and 0 everywhere else', () => {
        const matrix = createIdentityMatrix(4);

        const expected = createMatrix([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]);

        expect(areEqual(matrix, expected)).toBeTruthy();
    });
});