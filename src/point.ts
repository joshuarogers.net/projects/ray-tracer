import { isPracticallyEqual } from "./math";
import { createVector, Vector } from "./vector";

export type Point = [x: number, y: number, z: number];
export const createPoint = (x: number, y: number, z: number): Point => [x, y, z];

export const translatePoint = ([x1, y1, z1]: Point, [x2, y2, z2]: Vector) => createPoint(x1 + x2, y1 + y2, z1 + z2);
export const areEqual = ([x1, y1, z1]: Point, [x2, y2, z2]: Point): boolean =>
    isPracticallyEqual(x1, x2) && isPracticallyEqual(y1, y2) && isPracticallyEqual(z1, z2);

export const getTranslationVector = ([x1, y1, z1]: Point, [x2, y2, z2]: Point): Vector => createVector(x2 - x1, y2 - y1, z2 - z1);
