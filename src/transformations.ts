import { reverse } from "lodash";
import { createMatrix, multiply, type Matrix } from "./matrix";
import { createPoint, Point } from "./point";
import { createRay, Ray } from "./ray";
import { createVector, Vector } from "./vector";

export const identity = (): Matrix => createMatrix([
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1]
]);

export const translate = (x: number, y: number, z: number): Matrix => createMatrix([
    [1, 0, 0, x],
    [0, 1, 0, y],
    [0, 0, 1, z],
    [0, 0, 0, 1]
]);

export const scale = (x: number, y: number, z: number): Matrix => createMatrix([
    [x, 0, 0, 0],
    [0, y, 0, 0],
    [0, 0, z, 0],
    [0, 0, 0, 1]
]);

export const rotate = (x: number, y: number, z: number): Matrix => {
    const sinX = Math.sin(x);
    const sinY = Math.sin(y);
    const sinZ = Math.sin(z);

    const cosX = Math.cos(x);
    const cosY = Math.cos(y);
    const cosZ = Math.cos(z);

    /**
     * For rotation, the book gave three distinct matrices that could be used, one for each of
     * the three axis of rotation. For my implementation though, I really just wanted to be able
     * to describe rotation in one go by calling a single `rotate(x, y, z)`. I could have
     * achieved this by using the three rotation matrix the book provided and chaining them
     * through a multiply. What I wanted to do though was to use just a single matrix so that
     * I wouldn't have to perform two matrix multiplications just to generate the rotation matrix.
     * 
     * To that end, I decided to hit the internet. Wikipedia provided me with the following matrix
     * which, unbeknownst to me, was for a right-handed coordinate system.
     * 
     *   return createMatrix([
     *       [cosX * cosY, cosX * sinY * sinZ - sinX * cosZ, cosX * sinY * cosZ + sinX * sinZ, 0],
     *       [sinX * cosY, sinX * sinY * sinZ + cosX * cosZ, sinX * sinY * cosZ - cosX * sinZ, 0],
     *       [-sinY, cosX * sinZ, cosY * cosZ, 0],
     *       [0, 0, 0, 1]
     *   ]);
     * 
     * After getting my tests in place, I found that it didn't have the expected affects at all.
     * At first I tried massaging the inputs and outputs of the rotation matrix, but quickly
     * realized that I was getting well beyond my depths in hackery. After that realization I
     * sat down with some coffee, took the three rotation matrices provided by the book, and
     * multiplied them together manually to get the actual matrix being returned.
     */

    return createMatrix([
        [cosY * cosZ, sinX * sinY * cosZ + cosX * -sinZ, cosX * sinY * cosZ + sinX * -sinZ, 0],
        [cosY * sinZ, sinX * sinY * sinZ + cosX * cosZ, cosX * sinY * sinZ - sinX * cosZ, 0],
        [-sinY, sinX * cosY, cosX * cosY, 0],
        [0, 0, 0, 1]
    ]);
};

export interface SkewParameters {
    xByY?: number,
    xByZ?: number,
    yByX?: number,
    yByZ?: number,
    zByX?: number,
    zByY?: number
}

export const skew = ({xByY = 0, xByZ = 0, yByX = 0, yByZ = 0, zByX = 0, zByY = 0} :SkewParameters): Matrix => createMatrix([
    [1, xByY, xByZ, 0],
    [yByX, 1, yByZ, 0],
    [zByX, zByY, 1, 0],
    [0, 0, 0, 1]
]);

export const chain = (matrices: Matrix[]): Matrix => {
    if (matrices.length === 0)
        throw new Error('No transformations were provided to chain');
    
    const invalidMatrices = matrices.filter(x => x.getWidth() !== 4 || x.getHeight() !== 4);
    if (invalidMatrices.length > 0) {
        const invalid = invalidMatrices[0];
        throw new Error(`Cannot chain spatial transformation with size (${invalid.getWidth()}x${invalid.getHeight()})`);
    }

    const applicationOrder = reverse(matrices);
    return applicationOrder.slice(1).reduce(multiply, applicationOrder[0]);
};

export const projectRay = (ray: Ray, transformation: Matrix): Ray => {
    return createRay(
        projectPoint(ray.position, transformation),
        projectVector(ray.direction, transformation)
    );
};

export const projectPoint = ([x, y, z]: Point, transformation: Matrix): Point => {
    const source = createMatrix([[x], [y], [z], [1]]);
    const result = multiply(transformation, source);
    return createPoint(result.getValue(0, 0), result.getValue(0, 1), result.getValue(0, 2));
};

export const projectVector = ([x, y, z]: Vector, transformation: Matrix): Vector => {
    const source = createMatrix([[x], [y], [z], [0]]);
    const result = multiply(transformation, source);
    return createVector(result.getValue(0, 0), result.getValue(0, 1), result.getValue(0, 2));
};
