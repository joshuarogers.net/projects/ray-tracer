import { EPSILON } from "./math";
import { areEqual, createPoint, getTranslationVector, translatePoint } from "./point";
import { createVector } from "./vector";

describe('Points', () => {
    test('should be considered equal if they are nearly identical', () => {
        const point1 = createPoint(1, 2, 3);
        const point2 = createPoint(1 + EPSILON / 2, 2 + EPSILON / 2, 3 + EPSILON / 2);
        expect(areEqual(point1, point2)).toBeTruthy();
    });

    test('should be considered equal if they are nearly identical', () => {
        const point1 = createPoint(1, 2, 3);
        const point2 = createPoint(1 - EPSILON / 2, 2 - EPSILON / 2, 3 - EPSILON / 2);
        expect(areEqual(point1, point2)).toBeTruthy();
    });

    test('should not be considered equal if "x" is not nearly identical', () => {
        const point1 = createPoint(1, 2, 3);
        const point2 = createPoint(1 + EPSILON * 2, 2, 3);
        expect(areEqual(point1, point2)).toBeFalsy();        
    });

    test('should not be considered equal if "y" is not nearly identical', () => {
        const point1 = createPoint(1, 2, 3);
        const point2 = createPoint(1, 2 + EPSILON * 2, 3);
        expect(areEqual(point1, point2)).toBeFalsy();        
    });

    test('should not be considered equal if "z" is not nearly identical', () => {
        const point1 = createPoint(1, 2, 3);
        const point2 = createPoint(1, 2, 3 + EPSILON * 2);
        expect(areEqual(point1, point2)).toBeFalsy();        
    });

    test('should be ordered as x, y, z', () => {
        const [x, y, z] = createPoint(1, 2, 3);
        expect([x, y, z]).toEqual([1, 2, 3]);
    });

    test('should be translatable by a vector', () => {
        const point = createPoint(1, 2, 4);
        const vector = createVector(2, 5, -2);
        const [x, y, z] = translatePoint(point, vector);
        expect([x, y, z]).toEqual([3, 7, 2]);
    });

    test('should have a known vector between them', () => {
        const source = createPoint(1, 2, 5);
        const destination = createPoint(-3, 1, 6);
        const [x, y, z] = getTranslationVector(source, destination);
        expect([x, y, z]).toEqual([-4, -1, 1]);
    });
});

export {};
