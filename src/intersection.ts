import { chain } from "lodash";
import { type Shape } from "./geometry/shape";

export interface Intersection {
    time: number,
    target: Shape
}

export const getNextIntersection = (intersections: Intersection[]): Intersection | null => {
    return chain(intersections)
        .filter(x => x.time >= 0)
        .orderBy(x => x.time, "asc")
        .first()
        .value() || null;
};
