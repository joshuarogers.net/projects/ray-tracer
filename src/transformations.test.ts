import { areEqual as areMatricesEqual, createMatrix } from "./matrix";
import { areEqual as arePointsEqual, createPoint } from "./point";
import { createRay } from "./ray";
import { chain, projectPoint, projectRay, projectVector, rotate, scale, skew, translate } from "./transformations";
import { areEqual as areVectorsEqual, createVector } from "./vector";

describe('Projections', () => {
    test('can translate a point in space', () => {
        const source = createPoint(1, 2, 3);
        const matrix = translate(2, 4, -8);
        const target = createPoint(3, 6, -5);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('cannot translate a vector in space', () => {
        const source = createVector(1, 2, 3);
        const matrix = translate(2, 4, -8);
        const target = createVector(1, 2, 3);

        expect(areVectorsEqual(projectVector(source, matrix), target)).toBeTruthy();
    });

    test('can scale a point in space', () => {
        const source = createPoint(1, 2, 3);
        const matrix = scale(2, 4, -8);
        const target = createPoint(2, 8, -24);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can scale a vector in space', () => {
        const source = createVector(1, 2, 3);
        const matrix = scale(2, 4, -8);
        const target = createVector(2, 8, -24);

        expect(areVectorsEqual(projectVector(source, matrix), target)).toBeTruthy();
    });

    test('can rotate a point around the X-axis', () => {
        const fourtyFiveDegrees = Math.PI / 4;
        const source = createPoint(0, 1, 0);
        const matrix = rotate(fourtyFiveDegrees, 0, 0);
        const target = createPoint(0, Math.SQRT2 / 2, Math.SQRT2 / 2);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can rotate a vector around the X-axis', () => {
        const ninetyDegrees = Math.PI / 2;
        const source = createVector(0, 1, 0);
        const matrix = rotate(ninetyDegrees, 0, 0);
        const target = createVector(0, 0, 1);

        expect(areVectorsEqual(projectVector(source, matrix), target)).toBeTruthy();
    });

    test('can rotate a point around the Y-axis', () => {
        const fourtyFiveDegrees = Math.PI / 4;
        const source = createPoint(0, 0, 1);
        const matrix = rotate(0, fourtyFiveDegrees, 0);
        const target = createPoint(Math.SQRT2 / 2, 0, Math.SQRT2 / 2);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can rotate a vector around the Y-axis', () => {
        const ninetyDegrees = Math.PI / 2;
        const source = createVector(0, 0, 1);
        const matrix = rotate(0, ninetyDegrees, 0);
        const target = createVector(1, 0, 0);

        expect(areVectorsEqual(projectVector(source, matrix), target)).toBeTruthy();
    });

    test('can rotate a point around the Z-axis', () => {
        const fourtyFiveDegrees = Math.PI / 4;
        const source = createPoint(0, 1, 0);
        const matrix = rotate(0, 0, fourtyFiveDegrees);
        const target = createPoint(-Math.SQRT2 / 2, Math.SQRT2 / 2, 0);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can rotate a vector around the Z-axis', () => {
        const ninetyDegrees = Math.PI / 2;
        const source = createVector(0, 1, 0);
        const matrix = rotate(0, 0, ninetyDegrees);
        const target = createVector(-1, 0, 0);
        
        expect(areVectorsEqual(projectVector(source, matrix), target)).toBeTruthy();
    });

    test('can skew a point X in regards to Y', () => {
        const source = createPoint(2, 3, 4);
        const matrix = skew({ xByY: 1 });
        const target = createPoint(5, 3, 4);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can skew a point X in regards to Z', () => {
        const source = createPoint(2, 3, 4);
        const matrix = skew({ xByZ: 1 });
        const target = createPoint(6, 3, 4);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can skew a point Y in regards to X', () => {
        const source = createPoint(2, 3, 4);
        const matrix = skew({ yByX: 1 });
        const target = createPoint(2, 5, 4);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can skew a point Y in regards to Z', () => {
        const source = createPoint(2, 3, 4);
        const matrix = skew({ yByZ: 1 });
        const target = createPoint(2, 7, 4);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can skew a point Z in regards to X', () => {
        const source = createPoint(2, 3, 4);
        const matrix = skew({ zByX: 1 });
        const target = createPoint(2, 3, 6);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can skew a point Z in regards to Y', () => {
        const source = createPoint(2, 3, 4);
        const matrix = skew({ zByY: 1 });
        const target = createPoint(2, 3, 7);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can be chained together', () => {
        const source = createPoint(1, 0, 1);
        const matrix = chain([
            rotate(Math.PI / 2, 0, 0),
            scale(5, 5, 5),
            translate(10, 5, 7)
        ]);
        const target = createPoint(15, 0, 7);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('with only one chained item are identical to the single item', () => {
        const source = rotate(0, Math.PI / 4, 0);
        expect(areMatricesEqual(source, chain([source]))).toBeTruthy();
    });

    test('must have at least one item to be chained', () => {
        expect(() => chain([])).toThrowError('No transformations were provided to chain');
    });

    test('must be 4x4 to be chained', () => {
        expect(() => chain([
            translate(1, 2, 3),
            createMatrix([
                [1, 2, 3],
                [4, 5, 6]
            ]),
            rotate(0, 2, 1)
        ])).toThrowError('Cannot chain spatial transformation with size (3x2)');
    });
});

describe('Projecting', () => {
    test('can be applied to a point', () => {
        const source = createPoint(5, -3, 2);

        // Simply swaps the X and Z coordinates.
        const matrix = createMatrix([
            [0, 0, 1, 0],
            [0, 1, 0, 0],
            [1, 0, 0, 0],
            [0, 0, 0, 1]
        ]);
        const target = createPoint(2, -3, 5);

        expect(arePointsEqual(projectPoint(source, matrix), target)).toBeTruthy();
    });

    test('can translate a ray', () => {
        const ray = createRay(createPoint(1, 2, 3), createVector(0, 1, 0));
        const matrix = translate(3, 4, 5);
        const newRay = projectRay(ray, matrix);

        expect(newRay).not.toBe(ray);
        expect(arePointsEqual(newRay.position, createPoint(4, 6, 8))).toBeTruthy();
        expect(areVectorsEqual(newRay.direction, createVector(0, 1, 0))).toBeTruthy();
    });

    test('can scale a ray', () => {
        const ray = createRay(createPoint(1, 2, 3), createVector(0, 1, 0));
        const matrix = scale(2, 3, 4);
        const newRay = projectRay(ray, matrix);

        expect(newRay).not.toBe(ray);
        expect(arePointsEqual(newRay.position, createPoint(2, 6, 12))).toBeTruthy();
        expect(areVectorsEqual(newRay.direction, createVector(0, 3, 0))).toBeTruthy();
    });
})

export {};
